/*!
 * Bit&Black CSS Handler
 *
 * @copyright Copyright (c) 2021, Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

export { fadeIn, fadeOut } from "./src/Animation";
export { getStyle } from "./src/Style";
