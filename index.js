"use strict";
/*!
 * Bit&Black CSS Handler
 *
 * @copyright Copyright (c) 2021, Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
exports.__esModule = true;
exports.getStyle = exports.fadeOut = exports.fadeIn = void 0;
var Animation_1 = require("./src/Animation");
__createBinding(exports, Animation_1, "fadeIn");
__createBinding(exports, Animation_1, "fadeOut");
var Style_1 = require("./src/Style");
__createBinding(exports, Style_1, "getStyle");
