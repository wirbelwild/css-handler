import "./example.scss";
import { fadeIn, fadeOut } from "../../index";

const testElement = document.querySelector("#testElement");

const buttonFadeIn = document.querySelector("#fadeIn");
buttonFadeIn.addEventListener("click", () => {
    console.log("Fade in");
    fadeIn(testElement);
});

const buttonFadeOut = document.querySelector("#fadeOut");
buttonFadeOut.addEventListener("click", () => {
    console.log("Fade out");
    fadeOut(testElement);
});