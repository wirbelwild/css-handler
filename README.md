[![Codacy Badge](https://app.codacy.com/project/badge/Grade/8beb9b57ee1d4a5eb8298b0cff0d1a6b)](https://www.codacy.com/bb/wirbelwild/css-handler/dashboard)
[![npm version](https://badge.fury.io/js/bitandblack-css-handler.svg)](https://badge.fury.io/js/bitandblack-css-handler)

# Bit&Black CSS Handler

Basic CSS handling and manipulation with JavaScript. 

Currently available:

-   `getStyle`: Returns a specific element style.
-   `fadeIn`: Fades in an element.
-   `fadeOut`: Fades out an element.

## Installation

This library is made for the use with Node. Add it to your project by running

````shell script
$ yarn add bitandblack-css-handler
````

or

````shell script
$ npm install bitandblack-css-handler
````

## Usage

Use the library like that:

````javascript
import { fadeIn } from "~bitandblack/css-handler";

const testElement = document.querySelector("#myTestElement");
fadeIn(testElement);
````

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).