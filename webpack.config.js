const Encore = require("@symfony/webpack-encore");
const isProduction = Encore.isProduction();

Encore
    .setOutputPath("example/build/")
    .setPublicPath("/")
    .addEntry("example", "./example/assets/example.js")
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!isProduction)
    .enableVersioning(false)
    .enableSassLoader()
    .disableSingleRuntimeChunk()
    .enableTypeScriptLoader()
;

module.exports = Encore.getWebpackConfig();
