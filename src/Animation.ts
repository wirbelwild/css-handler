/**
 * Bit&Black CSS Handler
 *
 * @copyright Copyright (c) 2021, Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

/**
 * Fades out an element.
 *
 * @param element {HTMLElement} The element to fade out.
 */
export const fadeOut = (element: HTMLElement) => {
    let opacity = 1;
    element.style.opacity = String(opacity);

    (function fade() {
        opacity -= 0.1;
        element.style.opacity = String(opacity);
        
        if (opacity <= 0) {
            element.style.display = "none";
            return;
        }
        
        requestAnimationFrame(fade);
    }());
};

/**
 * Fades in an element.
 *
 * @param element {HTMLElement} The html element to fade in.
 * @param display {string}      The display style. This is `block` per default.
 */
export const fadeIn = (element: HTMLElement, display: string = "block") => {
    let opacity = 0;
    element.style.opacity = String(opacity);
    element.style.display = display;

    (function fade() {
        opacity += 0.1;
        
        if (opacity < 1) {
            element.style.opacity = String(opacity);
            requestAnimationFrame(fade);
        }
    }());
};