/**
 * Bit&Black CSS Handler
 *
 * @copyright Copyright (c) 2021, Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
/**
 * Fades out an element.
 *
 * @param element {HTMLElement} The element to fade out.
 */
export declare const fadeOut: (element: HTMLElement) => void;
/**
 * Fades in an element.
 *
 * @param element {HTMLElement} The html element to fade in.
 * @param display {string}      The display style. This is `block` per default.
 */
export declare const fadeIn: (element: HTMLElement, display?: string) => void;
