"use strict";
/**
 * Bit&Black CSS Handler
 *
 * @copyright Copyright (c) 2021, Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
exports.__esModule = true;
exports.fadeIn = exports.fadeOut = void 0;
/**
 * Fades out an element.
 *
 * @param element {HTMLElement} The element to fade out.
 */
var fadeOut = function (element) {
    var opacity = 1;
    element.style.opacity = String(opacity);
    (function fade() {
        opacity -= 0.1;
        element.style.opacity = String(opacity);
        if (opacity <= 0) {
            element.style.display = "none";
            return;
        }
        requestAnimationFrame(fade);
    }());
};
exports.fadeOut = fadeOut;
/**
 * Fades in an element.
 *
 * @param element {HTMLElement} The html element to fade in.
 * @param display {string}      The display style. This is `block` per default.
 */
var fadeIn = function (element, display) {
    if (display === void 0) { display = "block"; }
    var opacity = 0;
    element.style.opacity = String(opacity);
    element.style.display = display;
    (function fade() {
        opacity += 0.1;
        if (opacity < 1) {
            element.style.opacity = String(opacity);
            requestAnimationFrame(fade);
        }
    }());
};
exports.fadeIn = fadeIn;
