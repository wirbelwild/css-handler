/**
 * Bit&Black CSS Handler
 *
 * @copyright Copyright (c) 2021, Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
/**
 * Returns the style of an element. This works also with media properties.
 *
 * @param element {HTMLElement} The html element.
 * @param property {string}     The css property.
 * @return {string|null|*}
 */
export declare const getStyle: (element: HTMLElement, property: string) => string | null | any;
