"use strict";
/**
 * Bit&Black CSS Handler
 *
 * @copyright Copyright (c) 2021, Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */
exports.__esModule = true;
exports.getStyle = void 0;
/**
 * Returns the style of an element. This works also with media properties.
 *
 * @param element {HTMLElement} The html element.
 * @param property {string}     The css property.
 * @return {string|null|*}
 */
var getStyle = function (element, property) {
    if (!window.getComputedStyle) {
        return null;
    }
    return document
        .defaultView
        .getComputedStyle(element, null)
        .getPropertyValue(property);
};
exports.getStyle = getStyle;
